# -*- conding: utf-8 -*-

"""
    application app Tracking
    by Plus Digital
"""


from flask import Flask, redirect, session
from flask_appconfig.env import from_envvars


app = Flask('coreadmin')
from_envvars(app.config, prefix='FLASK_')


from applications.ws.views import app as register_user
app.register_blueprint(register_user) 