# -*- conding: utf-8 -*-

"""
    application app tracking movile
    by Plus Digital
"""

import datetime
import rethinkdb as r
import pprint
import json
from flask import Blueprint, render_template, abort, session, jsonify, request
from functools import update_wrapper
from datetime import timedelta
from flask import make_response, request, current_app
from time import gmtime, strftime

app = Blueprint('register_user', __name__)

#CROSSDOMAIN - DECORADOR HTML
def crossdomain(origin=None, methods=None, headers=None,max_age=21600, attach_to_all=True,automatic_options=True):
    if methods is not None:
        methods = ', '.join(sorted(x.upper() for x in methods))
    if headers is not None and not isinstance(headers, basestring):
        headers = ', '.join(x.upper() for x in headers)
    if not isinstance(origin, basestring):
        origin = ', '.join(origin)
    if isinstance(max_age, timedelta):
        max_age = max_age.total_seconds()
 
    def get_methods():
        if methods is not None:
            return methods
 
        options_resp = current_app.make_default_options_response()
        return options_resp.headers['allow']
 
    def decorator(f):
        def wrapped_function(*args, **kwargs):
            if automatic_options and request.method == 'OPTIONS':
                resp = current_app.make_default_options_response()
            else:
                resp = make_response(f(*args, **kwargs))
            if not attach_to_all and request.method != 'OPTIONS':
                return resp
 
            h = resp.headers
 
            h['Access-Control-Allow-Origin'] = origin
            h['Access-Control-Allow-Methods'] = get_methods()
            h['Access-Control-Max-Age'] = str(max_age)
            if headers is not None:
                h['Access-Control-Allow-Headers'] = headers
            return resp
 
        f.provide_automatic_options = False
        return update_wrapper(wrapped_function, f)
    return decorator


@app.route('/ajax/create/user/', methods=['POST', 'OPTIONS'])
@crossdomain(origin='*')
def create():

    reponse = {}
    conn = r.connect(host=current_app.config['RETHINKDB_HOST'])

    data = json.loads(request.data)

    data = {
        'name': data['name'],
        'last_name': data['last_name'],
        'user_name': data['user_name'],
        'email': data['email'],
        'date': data['date'],
        'asig_movile': [],
        'tracking': [],
        'warning': [],
        'binnacle': [],
        'status': ['sin asignar'] 
    }

    check_user = r.db(current_app.config['DATABASE']).table('users').filter({'email': data['email']}).run(conn)
    check_user = list(check_user)
    if check_user:
        reponse['success'] = 200
        reponse['message'] = u'El Usuario ya se encuentra registrado'
        reponse['code'] = 1
    else:
        insert  = r.db(current_app.config['DATABASE']).table('users').insert(data).run(conn)
        reponse['success'] = 200
        reponse['message'] = u'Ok'
        reponse['code'] = 0

    return jsonify(reponse)


@app.route('/ajax/all-users/', methods=['GET', 'OPTIONS'])
@crossdomain(origin='*')
def get_all_user():

    response = {}
    conn = r.connect(host=current_app.config['RETHINKDB_HOST'])

    all_user = r.db(current_app.config['DATABASE']).table('users').run(conn)
    all_user = list(all_user)
    response['success'] = 200
    response['data'] = all_user

    return jsonify(response)


@app.route('/ajax/get-coords-movile/', methods=['POST', 'OPTIONS'])
@crossdomain(origin='*')
def coords():

    response = {}
    conn = r.connect(host=current_app.config['RETHINKDB_HOST'])
    get_coords = json.loads(request.data)

    position = {
        'uuid': get_coords['device'],
        'coords' : get_coords['position'],
        'hour': strftime("%Y-%m-%d %H:%M:%S", gmtime())
    }

    save_coords = r.db(current_app.config['DATABASE']).table('asignation').filter({'uuid': data['uuid']}).run(conn)
    
    response['success'] = 200
    response['message'] = u'success'

    return jsonify(response)


@app.route('/ajax/create/device/', methods=['POST', 'OPTIONS'])
@crossdomain(origin='*')
def create_device():

    response = {}
    conn = r.connect(host=current_app.config['RETHINKDB_HOST'])

    data = json.loads(request.data)
    pprint.pprint(data)
    data = {
        'company': data['company'],
        'number_phone': data['number_phone'],
        'uuid': data['uuid'],
        'imei': data['imei'],
        'state': data['state'],
        'date': data['date'], 
        'status': ['sin asignar']
    }

    check_movile = r.db(current_app.config['DATABASE']).table('movile').filter({'imei': data['imei']}).run(conn)
    check_movile = list(check_movile)
    if check_movile:
        response['success'] = 200
        response['message'] = u'El dispositivo ya se encuentra registrado'
        response['code'] = 1
    else:
        insert  = r.db(current_app.config['DATABASE']).table('movile').insert(data).run(conn)
        response['success'] = 200
        response['message'] = u'Ok'
        response['code'] = 0

    return jsonify(response)


@app.route('/ajax/get-all-movile/', methods=['GET', 'OPTIONS'])    
@crossdomain(origin='*')
def all_movile():

    response = {}

    conn = r.connect(host=current_app.config['RETHINKDB_HOST'])

    all_movile = r.db(current_app.config['DATABASE']).table('movile').run(conn)
    all_movile = list(all_movile)
    response['success'] = 200
    response['data'] = all_movile

    return jsonify(response)


@app.route('/ajax/get-movil/<getmovil>',)
@crossdomain(origin='*')
def get_movil(getmovil):

    response = {}

    conn = r.connect(host=current_app.config['RETHINKDB_HOST'])
    get_movile = r.db(current_app.config['DATABASE']).table('movile').filter({'imei': getmovil}).run(conn)
    get_movile = list(get_movile)
    
    response['success'] = 200
    response['data'] = get_movile

    return jsonify(response)

@app.route('/ajax/update/user', methods=['POST'])
@crossdomain(origin='*')
def edit_user():

    response = {}
    conn = r.connect(host=current_app.config['RETHINKDB_HOST'])

    data = json.loads(request.data)
    pprint.pprint(data)

    get_user = r.db(current_app.config['DATABASE']).table('users').filter({'name': data['user']}).update({'asig_movile': data['disp']}).run(conn)

    change_status_user = r.db(current_app.config['DATABASE']).table('users').filter({'name': data['user']}).update({'status': ['asigando']}).run(conn)
    change_status_movile = r.db(current_app.config['DATABASE']).table('movile').filter({'imei': data['disp']}).update({'status':['asignado']}).run(conn)
    
    get_user = list(get_user)
    pprint.pprint(get_user)
    return jsonify(response)


@app.route('/ajax/filter-users/', methods=['GET', 'OPTIONS'])
@crossdomain(origin='*')
def filter_user():

    response = {}
    
    conn = r.connect(host=current_app.config['RETHINKDB_HOST'])
    status_user = r.db(current_app.config['DATABASE']).table('users').filter({'status': ['sin asignar']}).run(conn)
    status_user = list(status_user)
    response['success'] = 200
    response['data'] = status_user
    return jsonify(response)


@app.route('/ajax/filter-users-asignation/', methods=['GET', 'OPTIONS'])
@crossdomain(origin='*')
def filter_user_asignation():

    response = {}
    
    conn = r.connect(host=current_app.config['RETHINKDB_HOST'])
    status_user = r.db(current_app.config['DATABASE']).table('users').filter({'status': ['asigando']}).run(conn)
    status_user = list(status_user)
    response['success'] = 200
    response['data'] = status_user

    return jsonify(response)

@app.route('/ajax/delete/user/', methods=['POST', 'OPTIONS'])
@crossdomain(origin='*')
def delete_user():

    response = {}
    data = json.loads(request.data)

    pprint.pprint(data)
    
    #conn = r.connect(host=current_app.config['RETHINKDB_HOST'])
    #user = r.db(current_app.config['DATABASE']).table('user').filter({'name': })

    return jsonify(response)




