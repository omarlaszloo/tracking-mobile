# -*- conding: utf-8 -*-

"""
    application app tracking
    by Plus digital
"""


import pprint
import rethinkdb as r
from main import app
from flask.ext.script import Manager
from flask import current_app, redirect, url_for, flash

manager = Manager(app)


#create database
@manager.command
def create_db():

    conn = r.connect(host=current_app.config['RETHINKDB_HOST'])

    r.db_create(current_app.config['DATABASE']).run(conn)
    print '--> success DATABASE'

    r.db(current_app.config['DATABASE']).table_create('users').run(conn)
    print '--> success table users'

    r.db(current_app.config['DATABASE']).table_create('movile').run(conn)
    print '--> success table movile'

    r.db(current_app.config['DATABASE']).table_create('asignation').run(conn)
    print '--> success table asignacion'


if __name__=='__main__':
    manager.run()

