

var app = angular.module('app.admin', ['services', 'ngMaterial'])

    .config(function($mdIconProvider) {
      $mdIconProvider
        .iconSet('social', '../../images/icons/icons/sets/smartphone-with-wireless-internet.svg', 24)
        .iconSet('device', 'img/icons/sets/device-icons.svg', 24)
        .iconSet('communication', 'img/icons/sets/communication-icons.svg', 24)
        .defaultIconSet('img/icons/sets/core-icons.svg', 24);
    })


    /*
        
        ################ Controller user connect #############################
    
    */

    .controller('userOnline', function ($scope, services) {


        /* get user status asignation */
        services.getUserAsignation().success(function (data, status){
            if(status == 200) {
                $scope.filterUser = data.data
            }
        })

    })


    /*
        
        ################ End Controller user connect ##########################
    
    */
    
    /*
        
        ################ Controller add User #############################
    
    */

    .controller('addUrserController', function ($scope, $window, services){

        services.getUsers().success(function (data, status){
        if(status == 200) {
                $scope.alluser = data.data
            }
        })

        $scope.registerUser = function() {

            $scope.user = {
                'name': $scope.name,
                'last_name': $scope.last_name,
                'user_name': $scope.user_name,
                'email': $scope.email,
                'date': $scope.date 
            }

            services.createUser($scope.user).success(function (data, status){
                if(data.code == 0){
                    var title = 'Usuario agregado'
                    var success = 'Ok'
                    showSuccessMessage()
                    $scope.$broadcast('transfer',{'message':'ok'});
                }
                if(data.code == 1){
                    var title = 'El usuario ya existe'
                    var text = data.message
                    showConfirmMessageConfig(title, text)
                }
            })
        }

        $scope.deleteUser = function() {
            console.log('function deleteUser')
            var user = $('.modal-content').attr('username')
            
            $scope.user = {
                'name': user
            }

            services.deleteUsr($scope.user).success(function (data, status){
                if(status == 200) {
                    $scope.$broadcast('transfer',{'message':'ok'});
                }
            })
        }

    })

    .controller('allUserController', function ($scope, services){

        $scope.$on('transfer',function(){
            services.getUsers().success(function (data, status){
            if(status == 200) {
                    $scope.alluser = data.data
                }
            })
        });



    })

    /*

        ###################### End Controlller User ##############################

    */

    /*
        ####################### controller for add-movil #########################
    */

    .controller('addMovileController', function ($scope, services){

        services.getAllMovile().success(function (data, status){
        if(status == 200) {
                $scope.allmovil = data.data
            }
        })
        
        
        $scope.atribute = {}
        
        $scope.registerMovile = function() {

            $scope.movile = {
                'company': $scope.company,
                'number_phone': $scope.number_phone,
                'uuid': $scope.uuid,
                'imei': $scope.imei,
                'state': $scope.state,
                'date': $scope.date
            }

            

            services.postDevice($scope.movile).success(function (data, status){
                if(data.code == 0){
                    
                    $scope.atribute['imei'] = $scope.imei
                    var title = 'Dispositivo agregado'
                    var success = 'Ok'
                    showSuccessMessage()

                    services.getMovile($scope.atribute['imei']).success(function (data, status){
                        if(status == 200) {
                            $scope.$broadcast('transfer',{'message': data.data});
                        }
                    })
                }
                if(data.code == 1){
                    var title = 'El Dispositivo ya existe'
                    var text = data.message
                    showConfirmMessageConfig(title, text)
                }
            })

        }

    })

    .controller('updateMovil', function ($scope, services){

        $scope.$on('transfer',function(){
            services.getAllMovile().success(function (data, status){
            if(status == 200) {
                    $scope.allmovil = data.data
                }
            })
        });

    })

    /*
        ####################### End controller for add-movil #########################
    */


     /*
        ####################### End controller for user active #########################
    */
    .controller('assigmentController', function ($scope, services, $element) {

        $scope.data = {};

        services.getfilterUsers().success(function (data, status){
            if(status == 200) {
                $scope.filterUser = data.data
            }
        })

        services.getUserAsignation().success(function (data, status){
            if(status == 200) {
                $scope.updateUser = data.data
            }
        })

        services.getAllMovile().success(function (data, status){
        if(status == 200) {
                $scope.allmovil = data.data
            }
        })

        $scope.asigMobil = function(){
            
            var username = $('#modalAssigUser').attr('username')
            var disp = $scope.data.group1

            $scope.edit = {
                'user': username,
                'disp': disp
            }

            services.ajaxAsigMobil($scope.edit).success(function (data, status){
                if(status == 200){
                    showSuccessMessage()
                    $scope.$broadcast('updateCtrl',{'message': 'ok'});
                }
            })
        }

    })


    .controller('subAssigmentController', function ($scope, services) {
        
        $scope.$on('updateCtrl', function(){
            services.getUserAsignation().success(function (data, status){
                console.log('1')
                if(status == 200) {
                    $scope.updateUser = data.data
                }
            })
        })
    })



    /*
        ####################### End controller for user active #########################
    */