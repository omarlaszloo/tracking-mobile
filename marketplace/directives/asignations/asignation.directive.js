



var asig = angular.module('asig.directive', [])

    .directive('asigMovile', function() {
        return {
            restrict: 'A',
            link: function(scope, element, attr) {

                element.bind('click', function(){
                    var user = element.attr('user')
                    $('#largeModalLabel').text('Asigación al usuario ' + user)
                    $('.user').attr('asiguser', user)
                    $('.modal-content').attr('username', user)
                    
                })
            }
        }
    })
