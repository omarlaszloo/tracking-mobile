

var nav = angular.module('nav.directive', [])
    
    .directive('colorNav', function (){
        return {

            restrict: 'E',
            templateUrl: '../../views/nav/nav.html'
        }
    })
