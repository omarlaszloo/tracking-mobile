

var nav = angular.module('nav.header', [])


	.directive('navHeader', function() {
		return{
			restrict: 'E',
			templateUrl: '../../views/nav/nav-header.html'
		}
	})