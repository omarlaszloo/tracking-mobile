

var menu  = angular.module('menu.directive', [])

    .directive('panelMenu', function() {
        return {
            restrict: 'E',
            templateUrl: '../../views/menu/menu.html'
        }
    })