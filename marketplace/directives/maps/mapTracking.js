


var map =  angular.module('maptracking', [])

    .directive('appTracking', function(){

        return {
            restrict: 'E',
            template: '<div></div>',
            replace: true,
            link: function(scope, element, attrs){

                var myLatLng = new google.maps.LatLng(28.070011, 83.24939)
                var  mapOptions = {
                    center: myLatLng,
                    zoom: 8
                }

                var map = new google.maps.Map(document.getElementById(attrs.id), mapOptions)

                var marker = new google.maps.Marker({
                    position: myLatLng,
                    title: 'Yo',
                    map: map,
                    draggable: true,
                    animation: google.maps.Animation.DROP,
                    icon: '../images/marker.png'
                });

                marker.setMap(map)
            }
        }
    })

