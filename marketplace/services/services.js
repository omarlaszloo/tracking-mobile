

var services = angular.module('services', [])


services.factory('services', function ($http, $window){

    var urlBase = 'http://16a5b05d.ngrok.io'
    var Services = {}

    // services POST
    Services.createUser = function(reqdata) {

        return $http.post(urlBase + '/ajax/create/user/', reqdata, {
            headers: {'Content-type': undefined}
        }) 
	}

    Services.postDevice = function(reqdata) {
        return $http.post(urlBase + '/ajax/create/device/', reqdata, {
            headers: {'Content-type': undefined}
        })
    }

    Services.ajaxAsigMobil = function(reqdata) {
        return $http.post(urlBase + '/ajax/update/user', reqdata, {
            headers: {'Content-type': undefined}
        })
    }

    Services.deleteUsr = function(reqdata) {
        return $http.post(urlBase + '/ajax/delete/user/', reqdata, {
            headers: {'Content-type': undefined}
        })
    }

    // services GET
    Services.getUsers = function() {
        return $http.get(urlBase + '/ajax/all-users/')
    }

    Services.getPoints = function() {
        return $http.get(urlBase + '/ajax/get-points')
    }

    Services.getAllMovile = function() {
        return $http.get(urlBase + '/ajax/get-all-movile/')
    }

    Services.getMovile = function(reqdata) {
        return $http.get(urlBase + '/ajax/get-movil/' + reqdata)
    }

    Services.getfilterUsers = function() {
        return $http.get(urlBase + '/ajax/filter-users/')
    }

    Services.getUserAsignation = function() {
        return $http.get(urlBase + '/ajax/filter-users-asignation/')
    }



    return Services

})