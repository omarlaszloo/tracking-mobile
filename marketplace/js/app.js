

var app = angular.module('app', [
        'nav.header', 
        'menu.directive', 
        'nav.directive', 
        'asig.directive',
        'ngMaterial', 
        'ngAnimate', 
        'ngMessages', 
        'ngAria', 
        'ngRoute', 
        'app.admin',
        'maptracking'
    ])

app.config(['$routeProvider', function($routeProvider){

    $routeProvider

    .when('/home', {
        templateUrl: '../views/home/home.html'
    })
   
    .when('/add-user', {
        templateUrl: '../views/addUser/add-user.html',
        controller: 'addUrserController'
    })

    .when('/add-movile', {
        templateUrl: '../views/addMovile/add-movile.html'
    })

    .when('/assignment', {
        templateUrl: '../views/assignment/assignment.html'
    })

    .when('/sign-up', {
        templateUrl: '../views/login/login.html'
    })

    .otherwise({
        redirecTo: '/'
    })
}])